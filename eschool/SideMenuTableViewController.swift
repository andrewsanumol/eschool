//
//  SideMenuTableViewController.swift
//  eschool
//
//  Created by ALIN ANDREWS on 09/06/19.
//  Copyright © 2019 Training. All rights reserved.
//

import UIKit
class SideMenuTableViewController: UITableViewController {

    @IBOutlet var LOGOVIEW: UIView!
    
    @IBOutlet var NOTOFICATIONSVIEW: UIView!
    @IBOutlet var STAFFSMSVIEW: UIView!
    @IBOutlet var VOICESMSVIEW: UIView!
    @IBOutlet var SENDSMSVIEW: UIView!
    @IBOutlet var SMSVIEW: UIView!
    @IBOutlet var EVENTVIEW: UIView!
    @IBOutlet var NEWSVIEW: UIView!
    @IBOutlet var companyLogo: UIImageView!
    
    @IBAction func barbuttonItemPressed(_ sender: UIBarButtonItem) {
        appearView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.companyLogo.layer.cornerRadius = self.companyLogo.frame.size.width / 2;
        self.companyLogo.clipsToBounds = true
        self.tableView.alpha = 0
        self.tableView.isHidden = true
    }
  
    // MARK: - Table view data source

   
    func appearView() {
       

        UIView.animate(withDuration: 0.9, animations: {
            self.tableView.alpha = 1
        }, completion: {
            finished in
            self.tableView.isHidden = false
                    self.LOGOVIEW.isHidden = false
                    self.LOGOVIEW.backgroundColor = UIColor.gray
                    self.NOTOFICATIONSVIEW.isHidden = false
                    self.NOTOFICATIONSVIEW.backgroundColor = UIColor.gray

                    self.STAFFSMSVIEW.isHidden = false
                    self.STAFFSMSVIEW.backgroundColor = UIColor.gray

                    self.VOICESMSVIEW.isHidden = false
                    self.VOICESMSVIEW.backgroundColor = UIColor.gray

                    self.SENDSMSVIEW.isHidden = false
                    self.SENDSMSVIEW.backgroundColor = UIColor.gray

            
                    self.SMSVIEW.isHidden = false
                    self.SMSVIEW.backgroundColor = UIColor.gray

                    self.EVENTVIEW.isHidden = false
                    self.EVENTVIEW.backgroundColor = UIColor.gray

                    self.NEWSVIEW.isHidden = false
                    self.NEWSVIEW.backgroundColor = UIColor.gray

        })
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0{
            return 1
        }else{
            return 8
        }
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
