//
//  ViewController.swift
//  eschool
//
//  Created by ALIN ANDREWS on 09/06/19.
//  Copyright © 2019 Training. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var lockimageview: UIView!
    
    @IBOutlet var activateButton: UIButton!
    @IBOutlet var lockImage: UIImageView!
    
    
    let width: CGFloat = 100
    let height: CGFloat = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lockimageview.layer.cornerRadius = self.lockimageview.frame.size.width / 2;
        self.lockimageview.clipsToBounds = true
        
        

        activateButton.layer.cornerRadius = 10
        self.lockImage.layer.shadowPath = UIBezierPath(rect: self.lockImage.bounds).cgPath
        self.lockImage.layer.shouldRasterize = true
        self.lockImage.layer.rasterizationScale = UIScreen.main.scale

        lockImage.layer.shadowRadius = 0
        lockImage.layer.shadowOffset = .zero
        lockImage.layer.shadowOpacity = 0.2
        
        // how far the bottom of the shadow should be offset
        
        
        // make the bottom of the shadow finish a long way away, and pushed by our X offset
     
        
        let shadowOffsetX: CGFloat = 1900
        let shadowPath = UIBezierPath()
        shadowPath.move(to: CGPoint(x: 20, y: height))
        shadowPath.addLine(to: CGPoint(x: width - 30, y: 10))
        shadowPath.addLine(to: CGPoint(x: width + shadowOffsetX, y: 2000))
        shadowPath.addLine(to: CGPoint(x: shadowOffsetX, y: 2000))
        lockImage.layer.shadowPath = shadowPath.cgPath
        
       
        
        view.backgroundColor = UIColor(red: 230 / 255, green: 126 / 255, blue: 34 / 255, alpha: 1.0)
        
     
        // Do any additional setup after loading the view, typically from a nib.
    }


}

